<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->id();
            $table->foreignId('game_id');
            $table->string('question');
            // text_aksara bisa null soalnya yang rarangken bisanya pake image
            $table->string('text_aksara')->nullable();
            $table->string('image')->nullable();
            $table->string('aksara_quest_image')->nullable();
            $table->string('answer_key');
            $table->string('mean_aksara')->nullable();
            $table->string('question_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
};
