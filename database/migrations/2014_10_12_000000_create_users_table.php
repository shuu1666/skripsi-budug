<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username')->unique();
            $table->string('image')->nullable(); //profile photo
            $table->integer('used_frame')->nullable(); //frame profile photo
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            $table->integer('currency')->default(0);
            
            //login streak
            $table->integer('login_streak')->default(0);
            $table->date('last_login')->nullable();
            
            //exp
            $table->integer('total_exp')->default(0);
            $table->integer('current_exp')->default(0);
            $table->integer('level_user')->default(0);

            $table->integer('level_completed')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
