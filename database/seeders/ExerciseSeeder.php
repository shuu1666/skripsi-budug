<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Exercise;

class ExerciseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // done
        $dasar_satu = [
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮃ',
                'answer_key' => 'a',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮉ',
                'answer_key' => 'eu',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮈ',
                'answer_key' => 'e',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮇ',
                'answer_key' => 'o',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮄ',
                'answer_key' => 'i',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮅ',
                'answer_key' => 'u',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮆ',
                'answer_key' => 'é',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮏ',
                'answer_key' => 'ja',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮔ',
                'answer_key' => 'na',
                'question_type' => 'question',
            ],
            [
                'game_id' => 1,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮕ',
                'answer_key' => 'pa',
                'question_type' => 'question',
            ],
        ];

        // done
        $dasar_dua = [
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮏ',
                'answer_key' => 'ja',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮔ',
                'answer_key' => 'na',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮛ',
                'answer_key' => 'ra',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮚ',
                'answer_key' => 'ya',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮊ',
                'answer_key' => 'ka',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮎ',
                'answer_key' => 'ca',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮌ',
                'answer_key' => 'ga',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮘ',
                'answer_key' => 'ba',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮞ',
                'answer_key' => 'sa',
                'question_type' => 'question',
            ],
            [
                'game_id' => 3,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮜ',
                'answer_key' => 'la',
                'question_type' => 'question',
            ],
        ];

        //done
        $master_satu = [
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/panghulu.jpg',
                'answer_key' => 'panghulu',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/pamepet.jpg',
                'answer_key' => 'pamepet',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/paneuleung.jpg',
                'answer_key' => 'paneuleung',
                'question_type' => 'question',

            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/panyuku.jpg',
                'answer_key' => 'panyuku',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/paneleng.jpg',
                'answer_key' => 'panéléng',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/panolong.jpg',
                'answer_key' => 'panolong',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/panyecek.jpg',
                'answer_key' => 'panyecek',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/panyakra.jpg',
                'answer_key' => 'panyakra',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/pamingkal.jpg',
                'answer_key' => 'pamingkal',
                'question_type' => 'question',
            ],
            [
                'game_id' => 2,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'image' => 'images/aksara_rarangken/pamaeh.jpg',
                'answer_key' => 'pamaéh',
                'question_type' => 'question',
            ],
        ];

        // not done
        $master_dua = [
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮠᮤᮜᮩᮓ᮪',
                'answer_key' => 'hileud',
                'mean_aksara' => 'ulat',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮠᮚᮙ᮪',
                'answer_key' => 'hayam',
                'mean_aksara' => 'ayam',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮘᮀᮊᮧᮀ',
                'answer_key' => 'bangkong',
                'mean_aksara' => 'kodok',
                'question_type' => 'question',

            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮘᮓᮊ᮪',
                'answer_key' => 'badak',
                'mean_aksara' => 'badak',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮏᮕᮒᮤ',
                'answer_key' => 'japati',
                'mean_aksara' => 'merpati',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮘᮥᮠᮚ',
                'answer_key' => 'buhaya',
                'mean_aksara' => 'buaya',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮞᮧᮃᮀ',
                'answer_key' => 'soang',
                'mean_aksara' => 'angsa',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮙᮨᮛᮤ',
                'answer_key' => 'meri',
                'mean_aksara' => 'bebek',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮜᮅᮊ᮪',
                'answer_key' => 'lauk',
                'mean_aksara' => 'ikan',
                'question_type' => 'question',
            ],
            [
                'game_id' => 4,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮅᮎᮤᮀ',
                'answer_key' => 'ucing',
                'mean_aksara' => 'kucing',
                'question_type' => 'question',
            ],
        ];

        // not done
        $master_tiga = [
            [
                'game_id' => 5,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮘᮀᮊᮧᮀ',
                'answer_key' => 'bangkong',
                'mean_aksara' => 'kodok',
                'question_type' => 'question',

            ],
            [
                'game_id' => 5,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮏᮕᮒᮤ',
                'answer_key' => 'japati',
                'mean_aksara' => 'merpati',
                'question_type' => 'question',
            ],
            [
                'game_id' => 5,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮞᮧᮃᮀ',
                'answer_key' => 'soang',
                'mean_aksara' => 'angsa',
                'question_type' => 'question',
            ],
            [
                'game_id' => 5,
                'question' => 'Pilihlah jawaban untuk aksara berikut',
                'text_aksara' => 'ᮜᮅᮊ᮪',
                'answer_key' => 'lauk',
                'mean_aksara' => 'ikan',
                'question_type' => 'question',
            ],
        ];

        $master_tiga_tulis = [
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara japati berikut ini',
                'image' => 'images/aksara_exercise/japati.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/japati1.jpg',
                'text_aksara' => 'ᮏᮕᮒᮤ',
                'answer_key' => 'hileud',
                'mean_aksara' => 'merpati',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara lauk berikut ini',
                'image' => 'images/aksara_exercise/lauk.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/lauk1.jpg',
                'text_aksara' => 'ᮜᮅᮊ᮪',
                'answer_key' => 'lauk',
                'mean_aksara' => 'ulat',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara hayam berikut ini',
                'image' => 'images/aksara_exercise/hayam.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/hayam1.jpg',
                'text_aksara' => 'ᮠᮚᮙ᮪',
                'answer_key' => 'hayam',
                'mean_aksara' => 'ayam',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara badak berikut ini',
                'image' => 'images/aksara_exercise/badak.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/badak1.jpg',
                'text_aksara' => 'ᮘᮓᮊ᮪',
                'answer_key' => 'badak',
                'mean_aksara' => 'badak',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara buhaya berikut ini',
                'image' => 'images/aksara_exercise/buhaya.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/buhaya1.jpg',
                'text_aksara' => 'ᮘᮥᮠᮚ',
                'answer_key' => 'buhaya',
                'mean_aksara' => 'buaya',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara meri berikut ini',
                'image' => 'images/aksara_exercise/meri.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/meri1.jpg',
                'text_aksara' => 'ᮙᮨᮛᮤ',
                'answer_key' => 'meri',
                'mean_aksara' => 'bebek',
                'question_type' => 'draw',
            ],
        ];

        $master_tiga_tulis_beda = [
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara badak berikut ini',
                'image' => 'images/aksara_exercise/badak.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/badak1.jpg',
                'text_aksara' => 'ᮘᮓᮊ᮪',
                'answer_key' => 'badak',
                'mean_aksara' => 'badak',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara bangkong berikut ini',
                'image' => 'images/aksara_exercise/bangkong.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/bangkong1.jpg',
                'text_aksara' => 'ᮘᮀᮊᮧᮀ',
                'answer_key' => 'bangkong',
                'mean_aksara' => 'kodok',
                'question_type' => 'draw',

            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara buhaya berikut ini',
                'image' => 'images/aksara_exercise/buhaya.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/buhaya1.jpg',
                'text_aksara' => 'ᮘᮥᮠᮚ',
                'answer_key' => 'buhaya',
                'mean_aksara' => 'buaya',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara hayam berikut ini',
                'image' => 'images/aksara_exercise/hayam.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/hayam1.jpg',
                'text_aksara' => 'ᮠᮚᮙ᮪',
                'answer_key' => 'hayam',
                'mean_aksara' => 'ayam',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara hileud berikut ini',
                'image' => 'images/aksara_exercise/hileud.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/hileud1.jpg',
                'text_aksara' => 'ᮠᮤᮜᮩᮓ᮪',
                'answer_key' => 'hileud',
                'mean_aksara' => 'ulat',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara japati berikut ini',
                'image' => 'images/aksara_exercise/japati.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/japati1.jpg',
                'text_aksara' => 'ᮏᮕᮒᮤ',
                'answer_key' => 'japati',
                'mean_aksara' => 'merpati',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara lauk berikut ini',
                'image' => 'images/aksara_exercise/lauk.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/lauk1.jpg',
                'text_aksara' => 'ᮜᮅᮊ᮪',
                'answer_key' => 'lauk',
                'mean_aksara' => 'ikan',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara meri berikut ini',
                'image' => 'images/aksara_exercise/meri.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/meri1.jpg',
                'text_aksara' => 'ᮙᮨᮛᮤ',
                'answer_key' => 'meri',
                'mean_aksara' => 'bebek',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara soang berikut ini',
                'image' => 'images/aksara_exercise/soang.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/soang1.jpg',
                'text_aksara' => 'ᮞᮧᮃᮀ',
                'answer_key' => 'soang',
                'mean_aksara' => 'angsa',
                'question_type' => 'draw',
            ],
            [
                'game_id' => 5,
                'question' => 'Tulislah aksara ucing berikut ini',
                'image' => 'images/aksara_exercise/ucing.jpg',
                'aksara_quest_image' => 'images/aksara_exercise/ucing1.jpg',
                'text_aksara' => 'ᮅᮎᮤᮀ',
                'answer_key' => 'ucing',
                'mean_aksara' => 'kucing',
                'question_type' => 'draw',
            ],
        ];

        Exercise::insert($dasar_satu);
        Exercise::insert($dasar_dua);
        Exercise::insert($master_satu);
        Exercise::insert($master_dua);
        Exercise::insert($master_tiga);
        Exercise::insert($master_tiga_tulis);
        // Exercise::insert($master_tiga_tulis_beda);
    }
}
