<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Game;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $game_levels = [
            [
                'level_name' => 'Bumi',
                'level_difficulty' => 'Dasar 1',
                'color_hex' => 'F8BC38',
                'image' => 'images/level_image/3bumi.png',
            ],
            [
                'level_name' => 'Mars',
                'level_difficulty' => 'Master 1',
                'color_hex' => '5417BF',
                'image' => 'images/level_image/4mars.png',
            ],
            [
                'level_name' => 'Venus',
                'level_difficulty' => 'Dasar 2',
                'color_hex' => '5CB3E4',
                'image' => 'images/level_image/2venus.png',
            ],
            // [
            //     'level_name' => 'Merkurius',
            //     'level_difficulty' => 'Dasar 3',
            //     'color_hex' => 'C262CB',
            //     'image' => 'images/level_image/1merkurius.png',
            // ],
            [
                'level_name' => 'Jupiter',
                'level_difficulty' => 'Master 2',
                'color_hex' => '136EA2',
                'image' => 'images/level_image/5jupiter.png',
            ],
            [
                'level_name' => 'Saturnus',
                'level_difficulty' => 'Master 3',
                'color_hex' => '25C443',
                'image' => 'images/level_image/6saturnus.png',
            ],
            // [
            //     'level_name' => 'Uranus',
            //     'level_difficulty' => 'Master 4',
            //     'color_hex' => 'F4B11F',
            //     'image' => 'images/level_image/7uranus.png',
            // ],
        ];
        
        Game::insert($game_levels);
    }
}
