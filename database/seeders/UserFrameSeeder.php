<?php

namespace Database\Seeders;

use App\Models\UserFrame;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserFrameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchased_frame = [
            [
                'user_id' => 1,
                'frame_id' => 1,
            ],
        ];

        UserFrame::insert($purchased_frame);
    }
}
