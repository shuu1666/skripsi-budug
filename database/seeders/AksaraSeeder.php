<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Aksara;

class AksaraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aksara_swara = [
            [
                'tipe_aksara' => 'swara',
                'aksara'=> 'ᮃ',
                'latin' => 'a'
            ],
            [
                'tipe_aksara' => 'swara',
                'aksara'=> 'ᮆ',
                'latin' => '&#232;'
            ],
            [
                'tipe_aksara' => 'swara',
                'aksara'=> 'ᮄ',
                'latin' => 'i'
            ],
            [
                'tipe_aksara' => 'swara',
                'aksara'=> 'ᮇ',
                'latin' => 'o'
            ],
            [
                'tipe_aksara' => 'swara',
                'aksara'=> 'ᮅ',
                'latin' => 'u'
            ],
            [
                'tipe_aksara' => 'swara',
                'aksara'=> 'ᮈ',
                'latin' => 'e'
            ],
            [
                'tipe_aksara' => 'swara',
                'aksara'=> 'ᮉ',
                'latin' => 'eu'
            ],
        ];

        $aksara_ngalagena = [
            [
                'tipe_aksara' => 'ngalagena',
                'aksara'=> 'ᮊ',
                'latin' => 'ka'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮌ',
                'latin' => 'ga'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮍ',
                'latin' => 'nga'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮎ',
                'latin' => 'ca'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮏ',
                'latin' => 'ja'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮑ',
                'latin' => 'nya'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮒ',
                'latin' => 'ta'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮓ',
                'latin' => 'da'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮔ',
                'latin' => 'na'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮕ',
                'latin' => 'pa'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮘ',
                'latin' => 'ba'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮙ',
                'latin' => 'ma'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮚ',
                'latin' => 'ya'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮛ',
                'latin' => 'ra'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮜ',
                'latin' => 'la'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮝ',
                'latin' => 'wa'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮞ',
                'latin' => 'sa'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮠ',
                'latin' => 'ha'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮖ',
                'latin' => 'fa'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮗ',
                'latin' => 'va'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮋ',
                'latin' => 'qa'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮟ',
                'latin' => 'xa'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮐ',
                'latin' => 'za'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮮ',
                'latin' => 'kha'
            ],
            [
                'tipe_aksara' => 'ngalagena',
                'aksara' => 'ᮯ',
                'latin' => 'sya'
            ],
        ];

        $aksara_rarangken = [
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'panghulu',
                'latin' => 'panghulu',
                'description' => 'mengubah vokal aksara dasar /a/ menjadi /i/.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'pamepet',
                'latin' => 'pamepet',
                'description' => 'mengubah vokal aksara dasar /a/ menjadi /e/.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'paneuleung',
                'latin' => 'paneuleung',
                'description' => 'mengubah vokal aksara dasar /a/ menjadi /eu/.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'panglayar',
                'latin' => 'panglayar',
                'description' => 'menambah konsonan /+r/ pada akhir aksara dasar.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'panyecek',
                'latin' => 'panyecek',
                'description' => 'menambah konsonan /+ng/ pada akhir aksara dasar.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'panyuku',
                'latin' => 'panyuku',
                'description' => 'mengubah vokal aksara dasar /a/ menjadi /u/.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'panyakra',
                'latin' => 'panyakra',
                'description' => 'menambah konsonan /+ra/ pada aksara dasar.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'panyiku',
                'latin' => 'panyiku',
                'description' => 'menambah konsonan /+la/ pada aksara dasar.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'paneleng',
                'latin' => 'pan&#232;l&#232;ng',
                'description' => 'mengubah vokal aksara dasar /a/ menjadi /&egrave;/.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'panolong',
                'latin' => 'panolong',
                'description' => 'mengubah vokal aksara dasar /a/ menjadi /o/.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'pamingkal',
                'latin' => 'pamingkal',
                'description' => 'menambah konsonan /+ya/ pada aksara dasar.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'pangwisad',
                'latin' => 'pangwisad',
                'description' => 'menambah konsonan /+h/ pada aksara dasar.',
            ],
            [
                'tipe_aksara' => 'rarangken',
                'aksara'=> 'pamaeh',
                'latin' => 'pama&#232;h',
                'description' => 'menghilangkan huruf vokal pada aksara dasar yang mendahuluinya.',
            ],
        ];

        Aksara::insert($aksara_ngalagena);
        Aksara::insert($aksara_swara);
        Aksara::insert($aksara_rarangken);
    }
}
