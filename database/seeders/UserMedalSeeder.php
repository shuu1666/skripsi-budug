<?php

namespace Database\Seeders;

use App\Models\UserMedal;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserMedalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $all_users_medals = [
            [
                'user_id' => 1,
                'medal_id' => 1,
                'gained' => 1,
                'showed' => 1,
            ],
            [
                'user_id' => 1,
                'medal_id' => 2,
                'gained' => 0,
                'showed' => 0,
            ],
            [
                'user_id' => 2,
                'medal_id' => 2,
                'gained' => 1,
                'showed' => 1,
            ],
        ];

        UserMedal::insert($all_users_medals);
    }
}
