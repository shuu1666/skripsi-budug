<?php

namespace Database\Seeders;

use App\Models\UserGame;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserGameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status_user1 = [
            [
                'user_id' => 1,
                'game_id' => 1,
                'lock_status' => 1,
                'done_status' => 1,
            ],
            [
                'user_id' => 1,
                'game_id' => 2,
                'lock_status' => 1,
                'done_status' => 1,
            ],
            [
                'user_id' => 1,
                'game_id' => 3,
                'lock_status' => 1,
                'done_status' => 1,
            ],
            [
                'user_id' => 1,
                'game_id' => 4,
                'lock_status' => 1,
                'done_status' => 1,
            ],
            [
                'user_id' => 1,
                'game_id' => 5,
                'lock_status' => 1,
                'done_status' => 1,
            ],
        ];

        $status_user2 = [
            [
                'user_id' => 2,
                'game_id' => 1,
                'lock_status' => 1,
                'done_status' => 1,
            ],
            [
                'user_id' => 2,
                'game_id' => 2,
                'lock_status' => 1,
                'done_status' => 1,
            ],
        ];

        $status_user3 = [
            [
                'user_id' => 3,
                'game_id' => 1,
                'lock_status' => 1,
                'done_status' => 1,
            ],
            [
                'user_id' => 3,
                'game_id' => 2,
                'lock_status' => 1,
                'done_status' => 1,
            ],
        ];

        UserGame::insert($status_user1);
        UserGame::insert($status_user2);
        UserGame::insert($status_user3);
    }
}
