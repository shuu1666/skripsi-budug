<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Aksara;
use App\Models\Frame;
use App\Models\Medal;
use App\Models\Game;
use App\Models\Exercise;

use App\Models\UserMedal;
use App\Models\UserFrame;
use App\Models\UserGame;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'natasha',
            'username' => 'ntsha',
            'image'=> 'images/user_profile/user1.jpg',
            'email' => 'michelle@gmail.com',
            'password' => Hash::make('asd12'),
            'currency' => 100,
            'total_exp' => 39,
            'current_exp' => 9,
            'level_user' => 3,
            // 'login_streak' => 7,
            // 'last_login' => '2024-04-01',
        ]);

        User::create([
            'name' => 'michelle satu',
            'username' => 'michelle1',
            'image'=> 'images/user_profile/user2.jpg',
            'email' => 'michelle1@gmail.com',
            'password' => Hash::make('asd12'),
            'currency' => 10,
            'total_exp' => 20,
            'current_exp' => 0,
            'level_user' => 2,
        ]);

        User::create([
            'name' => 'michelle dua',
            'username' => 'michelle2',
            'image'=> 'images/user_profile/user3.jpg',
            'email' => 'michelle2@gmail.com',
            'password' => Hash::make('asd12'),
            'currency' => 10,
            'total_exp' => 45,
            'current_exp' => 5,
            'level_user' => 4,
        ]);

        $this->call ([
            AksaraSeeder::class,
            FrameSeeder::class,
            GameSeeder::class,
            ExerciseSeeder::class,
            UserGameSeeder::class,
            UserFrameSeeder::class,
        ]);
    }
}
