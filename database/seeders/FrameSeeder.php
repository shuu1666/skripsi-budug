<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Frame;

class FrameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $frame_store = [
            [
                'name' => 'Geometric Frame',
                'image' => 'images/frames/skin1.png',
                'currency'=> 10,
            ],
            [
                'name' => 'Summer Frame',
                'image' => 'images/frames/skin2.png',
                'currency'=> 8,
            ],
            // [
            //     'name' => 'Blue-Gold Frame',
            //     'image' => 'images/frames/skin3.png',
            //     'currency'=> 8,
            // ],
        ];

        $frame_store2 = [
            [
                'name' => 'Blue-Gold Frame',
                'image' => 'images/frames/skin3.png',
                'currency'=> 0,
                'day_streak'=> 8,
            ],
        ];

        Frame::insert($frame_store);
        Frame::insert($frame_store2);
    }
}
