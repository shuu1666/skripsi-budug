@extends('layouts.index')

@section('container')
<div class="container mt-5 pt-5 mb-5">
    <div class="row row-cols-auto">
        @foreach ($list_frame_store as $frame_store)
        <div class="col mt-3 d-flex align-items-stretch" style="float: left !important;">
            <div class="card card-store-frame">
                <img src="{{ asset($frame_store->image) }}" class="card-img-top h-auto" alt="{{ $frame_store->name }}">

                <div class="card-body card-body-frame">
                    <form action="/store" method="POST">
                        @csrf
                        <input type="text" class="card-text card-text-store-style" placeholder="{{ $frame_store->name }}" name="frame_name" value="{{ $frame_store->name }}">
                        <div class="button-use-purchase-frame">
                            @if ($frame_store->purchased == null)
                                @if ($frame_store->currency == 0 )
                                    @if (auth()->user()->login_streak >= $frame_store->day_streak)

                                        <button type="submit" class="btn btn-purchase-frame" value="{{ $frame_store->day_streak }}" name="frame_daystreak">Claim Frame</button>
                                    @else
                                        <p class="card-text-store-status mb-1 me-2">{{ $frame_store->day_streak }} Login Streak</p>
                                        {{-- <button type="submit" class="btn btn-purchase-frame" value="{{ $frame_store->day_streak }}" name="frame_daystreak">{{ $frame_store->day_streak }} days</button> --}}
                                    @endif
                                @else
                                    <button type="submit" class="btn btn-purchase-frame" value="{{ $frame_store->currency }}" name="frame_currency">{{ $frame_store->currency }} <i class="fa-solid fa-star fa-star-store"></i></button>
                                @endif

                            @elseif($frame_store->purchased != null && auth()->user()->used_frame == $frame_store->id)
                                <p class="card-text-store-status mb-1 me-2">Used</p>
                            @else
                                <button type="button" onclick="useFrame('{{ $frame_store->id }}')" value="Use" class="btn btn-purchase-frame">Use</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <!-- Popup Notification Card Modal-->
    @if($errors->any() == true)
    <div class="modal fade" id="popup-error-modal" tabindex="-1" role="dialog" aria-labelledby="popup-error-modal">
        <div class="modal-dialog margin-auto" role="document">
            <div class="modal-content modal-style-card">

                <div class="card" style="width: 18rem;">
                    <h4 class="card-title pt-4 text-center">Sorry!</h4>

                    <div class="card-body modal-style-card-body">
                        <img src="{{ asset('images/sorry.png') }}" class="card-img-top" alt="sorry image">
                        <p class="card-text">Maaf {{ auth()->user()->name }}, transaksi frame gagal karena jumlah Star atau Login Streak kurang!</p>
                        <button id="continue" type="button" class="btn btn-outline-primary" style="width: 100%" data-bs-dismiss="modal">Lanjutkan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

<form action="/frameuse" id="frameSubmit" method="POST">
    @csrf
    <input type="hidden" name="frame_id" id="frame_id">
</form>
@endsection


@section('script')
    <script>
        $( document ).ready(function() {
            $('#popup-error-modal').modal('show');
        });
        
        function useFrame(val) {
            var frameId = val;

            document.getElementById('frame_id').value = val;
            console.log(frameId);

            $('#frameSubmit').submit();
        }
    </script>
@endsection