@extends('layouts.index')

@section('container')
    <div class="container mt-5 pt-2 mb-5">
        <h4 class="text-center text-capitalize mt-4 pt-4 pb-4">Aksara {{ $title }}</h4>

        <div class="row row-cols-auto ms-0 ms-sm-5 ps-md-3 ms-lg-4 ms-xl-5 ps-xl-4 row-card-aksara-style">
            <div class="col-1 mx-2 text-center mt-4">
                {{-- Rarangken > Swara > Ngalagena --}}
                @if ($title == 'ngalagena')
                    <a class="button-aksara-style" href="{{ url('/aksara/rarangken') }}"><i class="fa fa-regular fa-square-caret-left icon-aksara-style fa-hover-hidden "></i><i class="fa fa-solid fa-square-caret-left icon-aksara-style fa-hover-show"></i></a>
                @elseif($title == 'rarangken')
                    <a class="button-aksara-style" href="{{ url('/aksara/swara') }}"><i class="fa fa-regular fa-square-caret-left icon-aksara-style fa-hover-hidden "></i><i class="fa bfa-solid fa-square-caret-left icon-aksara-style fa-hover-show"></i></a>
                @else
                    <a class="button-aksara-style" href="{{ url('/aksara/ngalagena') }}"><i class="fa fa-regular fa-square-caret-left icon-aksara-style fa-hover-hidden "></i><i class="fa fa-solid fa-square-caret-left icon-aksara-style fa-hover-show"></i></a>
                @endif
                
            </div>

            <div class="col-8 col-sm-8 col-md-9 px-0 ps-sm-4 ps-lg-5 ps-xl-4 justify-content-center text-center">
                @foreach ($aksaras as $aksara)
                    @if ($aksara->tipe_aksara == 'ngalagena' || $aksara->tipe_aksara == 'swara' )
                        <div class="col mt-3 mx-2 mx-md-3" style="float: left !important;">
                            <div class="card mt-0 me-0 aksara-card-style">
                                <div class="card-body text-center" style="height:100%">
                                    <h1 class="aksara-font">{!! $aksara->aksara !!}</h1>
                                    <p class="card-text-aksara-style">{!! $aksara->latin !!}</p>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col mt-3 mx-3" style="float: left !important;">
                            <div class="card mt-0 me-0" style="width: 14rem; height: 10rem">
                                <div class="card-body text-center" style="height:100%">
                                    <img src="{{ asset('images/aksara_rarangken/'.$aksara->aksara.'.jpg')}}" alt="{{ $aksara->latin }}">
                                    <p class="card-text-aksara-style">{!! $aksara->latin !!}</p>
                                    <p style="font-size:9pt" class="card-text-aksara-style">{!! $aksara->description !!}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="col-1 mx-2 ps-0 ms-0 text-center mt-4">
                @if ($title == 'ngalagena')
                    <a class="button-aksara-style" href="{{ url('/aksara/swara') }}"><i class="fa fa-regular fa-square-caret-right icon-aksara-style fa-hover-hidden"></i><i class="fa fa-solid fa-square-caret-right icon-aksara-style fa-hover-show"></i></a>
                @elseif($title == 'swara')
                    <a class="button-aksara-style" href="{{ url('/aksara/rarangken') }}"><i class="fa fa-regular fa-square-caret-right icon-aksara-style fa-hover-hidden"></i><i class="fa fa-solid fa-square-caret-right icon-aksara-style fa-hover-show"></i></a>
                @else 
                    <a class="button-aksara-style" href="{{ url('/aksara/ngalagena') }}"><i class="fa fa-regular fa-square-caret-right icon-aksara-style fa-hover-hidden"></i><i class="fa fa-solid fa-square-caret-right icon-aksara-style fa-hover-show"></i></a>
                @endif
            </div>
        </div>
    </div>
@endsection
