@extends('layouts.ingame')

@section('container')
    <div class="container mt-3 complete-game-style">

        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col centered-items">
                        <img src="{{ asset('images/complete.png') }}" class="rounded mx-auto d-flex" alt="Gift" style="width: 70%">
                        <h2 class="yellow-bold-text">Level Terselesaikan!</h2>
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="info-rewards">
                            <!-- exp yang didapat default -->
                            <div class="row">
                                <div class="col col-descript-complete">
                                    @if ($correct_ans==10)
                                        <p class="blue-thin-text centered-items">Selamat! Kamu berhasil menjawab semua pertanyaan dengan benar.</p>
                                    @elseif($correct_ans >= 7)
                                        <p class="blue-thin-text centered-items">Semangat! Kamu bisa meraih nilai sempurna! Kamu berhasil menjawab {{ $correct_ans }} pertanyaan dengan benar.</p>
                                    @elseif($correct_ans < 7)
                                        <p class="blue-thin-text centered-items">Tingkatkan belajarmu! Kamu menjawab {{ $correct_ans }} pertanyaan dengan benar.</p>
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <br>
                            
                            <div class="row">
                                <h3 class="yellow-bold-text perolehan-text">Total Perolehan</h3>
                            </div>

                            <br>
                    
                            <!-- total exp yang didapat keseluruhan -->
                            <div class="row">
                                <div class="col col-descript-complete">
                                    <p class="blue-thin-text mb-1">Total EXP</p>
                                </div>
                                <div class="col col-reward-complete">
                                    <p class="blue-thin-text mb-1">{{ $totalExp }} EXP</p>
                                </div>
                            </div>
                    
                            <!-- star yang didapat -->
                            <div class="row">
                                <div class="col col-descript-complete">
                                    <p class="blue-thin-text reward-complete">Rewards</p>
                                </div>
                                <div class="col col-reward-complete">
                                    <p class="blue-thin-text reward-complete">{{ $totalStar }} <i class="fa-solid fa-star"></i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col centered-items">
                        <a class="btn btn-outline-secondary button-game-play button-game-lanjut" href="/" role="button">Lanjutkan</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection