@extends('layouts.ingame')

@section('container')
<div class="container mt-3 pt-3 mb-2 px-5">
    <div class="row">
        <div class="col-1">
            <button type="button" id="backToHome" class="btn button-back" data-bs-toggle="modal" data-bs-target="#popup-confirmation"><i class="fa-solid fa-caret-left" style="font-size:24px"></i></button>
        </div>
        <div class="col-10 py-3">
            <div class="progress">
                <div class="progress-bar progress-game" id="theprogressbar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
            </div>
        </div>
        <div class="col-1">
            <img class="game-image-style" src="{{ asset($planet_image) }}" alt="{{ $planet_name }}">
        </div>
    </div>
</div>

<div id="carousel-exercise" class="carousel slide carousel-exercise-style">
    <div class="carousel-inner">
        <div class="container mt-2 text-center" style="padding-top:2%;">
            @foreach ($exercises as $exercise)
                {{-- @dd($exercise->question_type) --}}
                @if ($loop->first)
                    @if ($exercise->question_type == 'question')
                        <div class="carousel-item active">
                            <div class="question">
                                <p style="font-size:170%">{{ $exercise->question }}</p>
                            </div>
                            <div class="aksara-question mt-4 pt-3">
                                @if ($exercise->text_aksara!=null)
                                    <p style="font-size: 600%;">{{ $exercise->text_aksara }}</p>
                                @else
                                    <img src="{{ asset($exercise->image) }}" width="50px" class="mb-4" alt="aksara_question">
                                @endif
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend mt-4 mx-auto">
                                    <!-- Button Select Answer-->
                                    @php
                                        $shuffledOptionsAns = $exercise_option[$exercise->id];
                                        shuffle($shuffledOptionsAns); // shuffle the options array
                                    @endphp

                                    @foreach ($shuffledOptionsAns as $option)
                                        <input type="radio" class="btn-check" name="answer{{ $exercise->id }}" id="answer{{ $exercise->id }}{{ $option[0] }}" value="{{ $option[0] }}" autocomplete="off">

                                        <!-- jadi count, kalau arraynya ada 2 return option[1] else '' -->
                                        <label class="btn btn-outline-secondary" for="answer{{ $exercise->id }}{{ $option[0] }}">{{ $option[0] }} <br> <span>{{ count($option) == 2 ? '('.$option[1].')':'' }}</span> </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @elseif($exercise->question_type == 'draw')
                        <div class="carousel-item active">
                            <div class="question">
                                <p style="font-size:170%">{{ $exercise->question }}</p>
                            </div>

                            <div class="canvas-aksara mt-4 pt-3" style="background-image: url('{{ asset($exercise->aksara_quest_image) }}') !important">
                                <canvas class="canvas" id="canvas{{$exercise->id}}" width="450" height="250"></canvas>
                            </div>

                            <button type="button" id="clearCanvas" onclick="clearCanvasById(`canvas`+{{$exercise->id}})" class="btn button-game-play button-game-undo"><i class="fa-solid fa-arrow-rotate-left"></i></button>
                        </div>
                    @endif
                @else
                    @if ($exercise->question_type == 'question')
                        <div class="carousel-item">
                            <div class="question">
                                <p style="font-size:170%">{{ $exercise->question }}</p>
                            </div>
                            <div class="aksara-question mt-4 pt-3">
                                @if ($exercise->text_aksara!=null)
                                <p style="font-size: 600%;">{{ $exercise->text_aksara }}</p>
                                @else
                                <img src="{{ asset($exercise->image) }}" width="50px" class="mb-4" alt="aksara_rarangken">
                                @endif
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend mt-4 mx-auto">
                                    <!-- Button Select Answer-->
                                    @php
                                    $shuffledOptionsAns = $exercise_option[$exercise->id];
                                    shuffle($shuffledOptionsAns); // shuffle the options array
                                    @endphp

                                    @foreach ($shuffledOptionsAns as $option)
                                    <input type="radio" class="btn-check" name="answer{{ $exercise->id }}" id="answer{{ $exercise->id }}{{ $option[0] }}" value="{{ $option[0] }}" autocomplete="off">
                                    <label class="btn btn-outline-secondary" for="answer{{ $exercise->id }}{{ $option[0] }}">{{ $option[0] }} <br> <span>{{ count($option) == 2 ? '('.$option[1].')':'' }}</span> </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @elseif($exercise->question_type == 'draw')
                        <div class="carousel-item">
                            <div class="question">
                                <p style="font-size:170%">{{ $exercise->question }}</p>
                            </div>

                            <div class="canvas-aksara mt-4 pt-3" style="background-image: url('{{ asset($exercise->aksara_quest_image) }}') !important">
                                <canvas class="canvas" id="canvas{{$exercise->id}}" width="450" height="250"></canvas>
                            </div>

                            <button type="button" id="clearCanvas" onclick="clearCanvasById(`canvas`+{{$exercise->id}})" class="btn button-game-play button-game-undo"><i class="fa-solid fa-arrow-rotate-left"></i></button>
                        </div>
                    @endif
                @endif
            @endforeach
        </div>
    </div>
</div>

<!-- If submit true show correct notif, else show wrong notif and true answer -->
<footer class="fixed-bottom sticky-footer-style">
    <div class="container-fluid next-back-button pt-5 pb-5" id="default_button_container">
        <div class="row text-center">
            <div class="col">
                <button class="btn btn-outline-secondary button-game-play button-game-lewati" onclick="checkAnswer()" type="button">Lewati</button>
            </div>
    
            <div class="col">
                <button class="btn btn-outline-secondary button-game-play button-game-lanjut" onclick="checkAnswer()" type="button">Lanjutkan</button>
            </div>
        </div>
    </div>
    
    <div class="container-fluid next-back-button pt-5 pb-5 d-none" id="show_ans_container">
        <div class="row text-center row-anskey-style">
            <div class="col-6">
                <div class="row ms-5 ps-4">
                    <div class="col-2">
                        <i id="mark-anskey" style="font-size:40px;color:#e34f4f;" class="fa-regular"></i>
                    </div>
                    <div class="col-9">
                        <h5 class="text-left" style="color:#e34f4f;" id="text-desc-jawaban"></h5>
                        <p class="text-left" style="color:#e34f4f;" id="text-desc-anskey"></p>
                    </div>
                </div>
            </div>
    
            <div class="col-6">
                <button class="btn btn-outline-secondary button-game-play button-game-lanjut" type="button" data-bs-target="#carousel-exercise" onclick="nextButton()">Lanjutkan</button>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade" id="popup-confirmation" tabindex="-1" role="dialog" aria-labelledby="popup-confirmation">
    <div class="modal-dialog margin-auto" role="document">
        <div class="modal-content modal-style-card">

            <div class="card" style="width: 18rem;">
                <h4 class="card-title pt-4 text-center">Exit</h4>

                <div class="card-body modal-style-card-body">
                    <img src="{{ asset('images/exit_game.jpg') }}" class="card-img-top" alt="exit image">
                    <p class="card-text">Apakah anda yakin untuk keluar dari permainan?</p>

                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>

                    <a href="{{ url('/home') }}">
                        <button type="button" class="btn btn-danger">Yes</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="/levelclear" id="levelclear" method="POST">
    @csrf
    <input type="hidden" name="fAns" id="fAns">
    <input type="hidden" name="gEx" id="gEx">
    <input type="hidden" name="gSt" id="gSt">
    <input type="hidden" name="game_id" id="game_id">
</form>
@endsection

@section('script')
<script>
    var data = JSON.parse('<?php echo $exercises ?>');

    var counter = 0;
    var falseAnswer = 0; // untuk check apakah user ada jawaban yang salah/tidak
    var progressBar = 0;

    var bonusExp = 0;
    var gainedExp = 0;
    var gainedStar = 0;

    var flag = [];
    var canvasIndex = [];

    let accuracy=0;
    let selectedCanvasId=0;

    // $(document).ready(function() {
    //     console.log("ready!");
    //     console.log(data);
    // });

    //function untuk rubah URL ke file yang nantinya diproses di API 
    const dataURLtoFile = (dataurl, filename) => {
        const arr = dataurl.split(',')
        const mime = arr[0].match(/:(.*?);/)[1]
        const bstr = atob(arr[1])
        let n = bstr.length
        const u8arr = new Uint8Array(n)
        while (n) {
            u8arr[n - 1] = bstr.charCodeAt(n - 1)
            n -= 1 // to make eslint happy
        }
        return new File([u8arr], filename, {
            type: mime
        })
    };

    // function untuk rubah url image jadi url base64
    function toDataURL(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
        var reader = new FileReader(); //untuk baca konten file (row data buffers)
            reader.onloadend = function() {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
            };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    };

    // looping canvas sebanyak pertanyaan di soalnya ada berapa, buat tau banyaknya get pake class
    const canvas = document.getElementsByClassName("canvas");
    for (var i = 0; i < canvas.length; i++) {
        const selectedCanvas = document.getElementById(canvas.item(i).id);
        canvasIndex.push(selectedCanvas.id);
        drawingCanvas(selectedCanvas);
    };

    function clearCanvas(){
        canvasIndex.forEach(function (canvasId){
            clearCanvasById(canvasId);
        });
    }

    function clearCanvasById(canvasId) {
        var canvas = document.getElementById(canvasId);
        if (canvas) {
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
        } else {
            console.error("Canvas with ID '" + canvasId + "' not found.");
        }
    }    

    // function biar canvas bisa di gambar pake mouse
    function drawingCanvas(canvasId) {
        let context = canvasId.getContext("2d");
        
        // Mengatur warna dan ketebalan garis
        context.strokeStyle = "#000";
        context.lineWidth = 7;
        context.lineCap = 'round';
        
        let isDrawing = false;

        // Fungsi untuk menggambar
        function draw(e) {
            if (!isDrawing) return;
            const x = (e.clientX - canvasId.getBoundingClientRect().left) * (canvasId.width / canvasId.offsetWidth);
            const y = (e.clientY - canvasId.getBoundingClientRect().top) * (canvasId.height / canvasId.offsetHeight);

            context.lineTo(x, y);
            context.stroke();
        }

        canvasId.addEventListener("mousedown", (e) => {
            isDrawing = true;
            context.beginPath();
            draw(e);
            canvasId.addEventListener("mousemove", draw);
        });

        canvasId.addEventListener("mouseup", () => {
            isDrawing = false;
            canvasId.removeEventListener("mousemove", draw);
        });

        canvasId.addEventListener("touchstart", (e) => {
            isDrawing = true;
            const x = e.touches[0].clientX - canvasId.getBoundingClientRect().left;
            const y = e.touches[0].clientY - canvasId.getBoundingClientRect().top;
            context.beginPath();
            context.moveTo(x, y);
        });

        canvasId.addEventListener("touchmove", (e) => {
            if (!isDrawing) return;
            const x = e.touches[0].clientX - canvasId.getBoundingClientRect().left;
            const y = e.touches[0].clientY - canvasId.getBoundingClientRect().top;
            context.lineTo(x, y);
            context.stroke();
        });

        canvasId.addEventListener("touchend", () => {
            isDrawing = false;
        });

        canvasId.addEventListener("touchcancel", () => {
            isDrawing = false;
        });
    }

    // function buat check match image nya
    // semakin mendekati 0 semakin bagus
    function checkAnswerDraw(similarity){
        if (similarity>=0.739) {
            console.log(similarity);
            return true;
        }
        console.log(similarity);
        return false;
    }

    function nextButton() {
        document.getElementById('default_button_container').classList.remove('d-none');
        document.getElementById('show_ans_container').classList.add('d-none');

        $("#carousel-exercise").carousel('next');

    }

    function checkAnswer() {
        if (data[counter].question_type == 'question') {
            var check = $("input[name=answer" + data[counter].id + "]:checked").val();
            // console.log('check status');
            // console.log(check);
            // console.log('anskey:' + data[counter].answer_key);
            if (check == data[counter].answer_key && !flag.includes(counter)) {
                // console.log(flag.includes(counter));

                gainedExp += 1;
                gainedStar += 1;

                flag.push(counter);
                $("#carousel-exercise").carousel('next');
            }else{
                falseAnswer += 1;

                document.getElementById('mark-anskey').classList.add('fa-circle-xmark');
                document.getElementById('text-desc-jawaban').innerHTML='Jawaban Benar:';
                document.getElementById('text-desc-anskey').innerHTML = data[counter].answer_key;
                document.getElementById('default_button_container').classList.add('d-none');
                document.getElementById('show_ans_container').classList.remove('d-none');
            }
        }else{
            // console.log('check image');
            // API to python image similarity ----- (/) -> host
            // http://127.0.0.1:8000/
            var imageUrl = '/' + data[counter].image; // image asli
            const canvasId = document.getElementById('canvas'+data[counter].id)
            // debugger;
            toDataURL(imageUrl, (realimage)=>{
                const imageCanvasUrl = canvasId.toDataURL("image/png", 1.0).replace("image/png", "image/jpg"); // image canvas
                const rimage = dataURLtoFile(realimage, 'realimage.jpg');
                const simage = dataURLtoFile(imageCanvasUrl, 'similarimage.jpg');
                
                var formData = new FormData();
                // nama var, file, filename 
                formData.append('realimage', rimage, rimage.name);
                formData.append('similarimage', simage, simage.name);

                // send ke API ----- (http://34.125.145.113:11000/similarity) -> host
                // send ke API ----- (https://skripsi-nyunda-api.beatfraps.com/similarity) -> host

                // local http://127.0.0.1:10000/similarity
                axios.post('http://skripsi-nyunda-api.beatfraps.com/similarity', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(({
                    data
                }) => {
                    if (checkAnswerDraw(data) == true && !flag.includes(counter)) {
                        gainedExp += 1;
                        gainedStar += 1;

                        flag.push(counter);
                        $("#carousel-exercise").carousel('next');
                    }else{
                        falseAnswer += 1;

                        document.getElementById('mark-anskey').classList.add('fa-circle-xmark');
                        document.getElementById('text-desc-jawaban').innerHTML='Penulisan kamu kurang tepat!';
                        document.getElementById('default_button_container').classList.add('d-none');
                        document.getElementById('show_ans_container').classList.remove('d-none');
                    } 
                });
            });
        }

        // console.log(flag);
        progressBar += 10;

        // naikin persentase progress bar
        $('#theprogressbar').attr('aria-valuenow', progressBar).css('width', progressBar + '%');
        document.getElementById('theprogressbar').innerHTML = progressBar + '% terjelajahi';


        if (progressBar == 100) {
            document.getElementById('fAns').value = falseAnswer;
            document.getElementById('gEx').value = gainedExp;
            document.getElementById('gSt').value = gainedStar;
            document.getElementById('game_id').value = data[0].game_id;

            $('#levelclear').submit();
        }

        counter++;
        if (counter == data.length) {
            counter = 0;
        }
    }
</script>
@endsection