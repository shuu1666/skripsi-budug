@auth
<nav class="navbar fixed-top navbar-expand-lg bg-body-tertiary py-0">
    <div class="container mt-2 mb-1">
        <a class="navbar-brand" href="#">Nyunda</a>

        <button class="navbar-toggler dropdown-toggle toggler-navbar-style" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="span-navbar-style"> <i class="fa-solid fa-bars"></i></span>
          </button>

        <div class="collapse navbar-collapse navbar-collapse-style" id="navbarNavDropdown">
            <div class="navbar-nav navbar-style">
                <div class="container text-center">
                    <div class="row">
                        <!-- Visible only on large -->
                        <div class="nav-item col ms-lg-2 my-auto d-none d-lg-block d-xl-block px-0">
                            <a class="nav-link nav-main-style px-0 {{ $active === 'aksara' ? 'active' : '' }}" href="{{ url('/aksara/ngalagena') }}"><i class="fa-solid fa-book"></i> Aksara</a>
                        </div>
                        <div class="nav-item col my-auto d-none d-lg-block d-xl-block px-0">
                            <a class="nav-link nav-main-style px-0 {{ $active === 'learn' ? 'active' : '' }}" href="{{ url('/home') }}"><i class="fa-solid fa-school"></i> Belajar</a>
                        </div>
                        <div class="nav-item col my-auto d-none d-lg-block d-xl-block px-0">
                            <a class="nav-link nav-main-style px-0 {{ $active === 'store' ? 'active' : '' }}" href="{{ url('/store') }}"><i class="fa-solid fa-store"></i> Toko</a>
                        </div>

                        <!-- Star and Profile -->
                        <div class="nav-item col-4 my-auto d-none d-lg-block d-xl-block px-0">
                            <div class="row">
                                <div class="col-sm-4 my-auto pe-0">
                                    <p class="star-navbar mb-0"><i class="fa-solid fa-star"></i> {{ auth()->user()->currency }}</p>
                                </div>

                                <div class="col-sm my-auto ps-0">
                                    <form action="/logout" method="POST">
                                        @csrf
                                        <button type="submit" class="active-dlink-style dlink-button-style my-auto mb-2 text-left-dd-style">
                                            <i class="logout-navbar fa-solid fa-right-from-bracket"></i> Logout
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Hidden only on large -->
                        <div class="nav-item col-md-12 my-auto d-lg-none d-xl-none text-left-dd-style">
                            <a class="nav-link active-dlink-style {{ $active === 'aksara' ? 'active' : '' }}" href="{{ url('/aksara/ngalagena') }}"><i class="fa-solid fa-book"></i> Aksara</a>

                            <a class="nav-link active-dlink-style {{ $active === 'learn' ? 'active' : '' }}" href="{{ url('/home') }}"><i class="fa-solid fa-school"></i> Belajar</a>

                            <a class="nav-link active-dlink-style {{ $active === 'store' ? 'active' : '' }}" href="{{ url('/store') }}"><i class="fa-solid fa-store"></i> Toko</a>

                            <form action="/logout" method="POST">
                                @csrf
                                <button type="submit" class="active-dlink-style dlink-button-style my-auto mb-2 text-left-dd-style">
                                    <i class="fa-solid fa-right-from-bracket"></i> Logout
                                </button>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>

@else

<nav class="navbar bg-body-tertiary navbar-light">
    <div class="container my-2 login-navbar-style pe-5">
        @if ($title == 'Login')
            <a class="nav-link" href="{{ url('/register') }}"><i class="fa-solid fa-right-to-bracket text-end"></i> Register</a>
        @else
            <a class="nav-link" href="{{ url('/login') }}"><i class="fa-solid fa-right-to-bracket text-end"></i> Login</a>
        @endif
    </div>
</nav>
@endauth
