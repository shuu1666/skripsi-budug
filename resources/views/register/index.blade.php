@extends('layouts.index')

@section('container')
<div class="container">
    <div class="row justify-content-center text-center">
        <div class="col-lg-5 col-md-7 col-sm-8 col-8">
            <main class="form-registration m-auto mt-5 pt-2">
                <h1 class="h3 mb-3 fw-normal">Create your profile</h1>

                <form action="/register" method="POST">
                    <!-- harus pake csrf_token nanti web kita akan generate token yang nantinya dicocokan dengan requestnya, kalau sama bisa -->
                    @csrf    
                    <div class="form-floating">
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Name" autofocus required value="{{ old('name') }}">
                        <label for="name">Name</label>

                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="pt-3"></div>

                    <div class="form-floating">
                        <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Username" required value="{{ old('username') }}">
                        <label for="username">Username</label>

                        @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="pt-3"></div>

                    <div class="form-floating">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="name@example.com" required value="{{ old('email') }}">
                        <label for="email">Email address</label>

                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="pt-3"></div>

                    <div class="form-floating">
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" required>
                        <label for="password">Password</label>

                        @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="pt-3"></div>
                
                    {{-- <div class="form-check text-start my-3">
                        <input class="form-check-input" type="checkbox" value="remember-me" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                        Remember me
                        </label>
                    </div> --}}

                    <!-- Mau di jadiin kaya notification bubble gitu -->
                    @if (session()->has('success'))
                        <p>
                            {{ session('success') }}
                        </p>    
                    @endif

                    <div class="pt-3"></div>
                    
                    <button class="btn btn-primary w-100 py-2 text-center" type="submit">Register</button>
                </form>
            </main>
        </div>
    </div>
</div>
@endsection