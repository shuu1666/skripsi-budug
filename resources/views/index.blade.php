@extends('layouts.index')

@section('container')
<div class="container mt-5 pt-5 mb-5">
    {{-- <h1>Halaman Belajar</h1> --}}
    <div class="row">

        <!-- Exp, leaderboards, medals, follow us -->
        <div class="col-lg-1 d-none d-xl-block"></div>
        <div class="col-xl-3 col-lg-4 d-none d-lg-block me-2">
            <div class="row">

                <!-- Exp -->
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title mb-2">EXP</h5>

                        <div class="row">
                            <div class="col">
                                <div class="card text-white" style="width: 75px; border: none;">
                                    <img class="card-img" src="{{ asset('images/level.png') }}" alt="Card image">
                                    <div class="card-img-overlay">
                                        <p class="card-title text-center mt-2 mx-auto mb-0"
                                            style="font-size: 11px; font-weight:bold">
                                            Lv.{{ auth()->user()->level_user }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col my-auto">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar"
                                        style="width: {{ auth()->user()->current_exp }}0%"
                                        aria-valuenow="{{ auth()->user()->current_exp }}" aria-valuemin="0"
                                        aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div class="col my-auto">
                                <p class="mb-2">{{ auth()->user()->current_exp }}/10</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Leaderboards -->
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title mb-2">Papan Peringkat</h5>                            
                        @if ($user_profile->count() > 3)
                            @for ($i = 0; $i < 3; $i++)
                                <div class="card mb-3 shadow shadow-style bg-white" style="height: 4rem;">
                                    <div class="card-body">
                                        <div class="row my-3">
                                            <div class="col-md-5">
                                                @if ($desc_rank[$i]->used_frame == null)
                                                    @if ($desc_rank[$i]->image == null)
                                                        <img src="{{ asset('images/user_profile/default_profile.jpg') }}"
                                                        class="mt-0 rounded-3 top-50 translate-middle ms-4"
                                                        style="width: 50px; height:50px;" alt="profile user ">
                                                    @else
                                                        <img src="{{ asset($desc_rank[$i]->image) }}"
                                                        class="mt-0 rounded-3 top-50 translate-middle ms-4"
                                                        style="width: 50px; height:50px;" alt="profile user ">
                                                    @endif
                                                @else
                                                    <div class="photo-figure-rank rounded-3 mt-0 translate-middle ms-4" style="background-image: url('{{ asset($desc_rank[$i]->image) }}') !important; ">
                                                        <img src="{{ asset($desc_rank[$i]->frame) }}" style="width: 50px; height:50px;" alt="profile user ">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-5 descrank-user-name">
                                                <p class="top-50 text-truncate translate-middle ms-2 text-capitalize text-left"> {{ $desc_rank[$i]->name }}</p>
                                            </div>
                                            <div class="col-md-2">
                                                <p class="position-absolute top-50 translate-middle ">
                                                    {{ $desc_rank[$i]->total_exp }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        @else
                        <!-- kalau usernya kurang dari 2 jadi tinggal foreach langsung -->
                            @foreach ($desc_rank as $user)
                                <div class="card mb-3" style="height: 4rem;">
                                    <div class="card-body">
                                        <div class="row my-3">
                                            <div class="col-md-5"> 
                                                @if ($user->image == null)
                                                    <img src="{{ asset('images/user_profile/default_profile.jpg') }}"
                                                    class="mt-0 rounded-3 top-50 translate-middle ms-4"
                                                    style="width: 50px; height:50px;" alt="profile user ">
                                                @else
                                                    <img src="{{ asset($user->image) }}"
                                                        class="mt-0 rounded-3 top-50 translate-middle ms-4"
                                                        style="width: 50px; height:50px;" alt="profile user ">
                                                @endif
                                            </div>
                                            <div class="col-md-5 descrank-user-name">
                                                <p class="top-50 text-truncate translate-middle ms-2 text-capitalize text-left"> {{ $user->name }}</p>
                                            </div>
                                            <div class="col-md-2">
                                                <p class="position-absolute top-50 translate-middle ">
                                                    {{ $user->total_exp }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>

                <!-- Medals -->
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title mb-2">Medali {{ auth()->user()->name }}</h5>

                        <div class="row">
                            <div class="col medal-images offset-md-3">
                                <img src="{{ asset($user_medals) }}" class="mt-0 img-thumbnail-medal" alt="{{ $medal_name }}">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Follow Us -->
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title mb-2">Ikuti Kami</h5>

                        <div class="row">
                            <div class="col mt-3">
                                <button type="button" class="btn btn-outline-primary text-truncate shadow shadow-style bg-white" style="width: 100%;">
                                    <p class="follow-style-button m-0">
                                        <i class="fab fa-twitter"></i> Twitter
                                    </p>
                                </button>
                            </div>
                            <div class="col mt-3">
                                <button type="button" class="btn btn-outline-primary text-truncate shadow shadow-style bg-white" style="width: 100%;">
                                    <p class="follow-style-button m-0">
                                        <i class="fab fa-instagram"></i> Instagram
                                    </p>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- profiles, game levels -->
        <div class="col-xs-12 col-sm-12 col-lg-7 col-xl-6 ms-5">
            <!-- Profiles -->
            <div class="card card-profile-style">
                <div class="card-body">
                    <h5 class="card-title card-title-profile d-none d-sm-block">Profiles</h5>
                    <h5 class="card-title card-title-profile d-block d-sm-none text-capitalize" width="100px">Hello, {{ auth()->user()->name }} !</h5>

                    <div class="container text-center">
                        <div class="row">
                            {{-- profile photo --}}
                            <div class="col-12 col-sm-auto position-relative">
                                @if(auth()->user()->image == null && auth()->user()->used_frame == null)
                                    <img src="{{ asset('images/user_profile/default_profile.jpg') }}" class="rounded profile-picture-style" width="130px" alt="profile picture">

                                    <button type="button" onclick="modalProfilePicture()" class="btn btn-secondary btn-pp-style btn-sm position-absolute m-md-2 mb-md-0 mb-lg-3 rounded-circle" data-toggle="modal" data-target="#changePicture" aria-hidden="true"><img src="{{ asset('images/icon_image/camera-solid.png') }}" class="profile_icon_style" alt="profile_picture_icon"></button>
                                @elseif(auth()->user()->used_frame == null)
                                    <img src="{{ asset(auth()->user()->image) }}" class="rounded profile-picture-style" width="130px" alt="profile picture">

                                    <button type="button" onclick="modalProfilePicture()" class="btn btn-secondary btn-pp-style btn-sm position-absolute m-md-2 mb-md-0 mb-lg-3 rounded-circle" data-toggle="modal" data-target="#changePicture" aria-hidden="true"><img src="{{ asset('images/icon_image/camera-solid.png') }}" class="profile_icon_style" alt="profile_picture_icon"></button>
                                @else
                                    <div class="photo-figure" style="background-image: url('{{ asset(auth()->user()->image) }}') !important; ">
                                        <img class="profile-picture-background-style" src="{{ asset($used_frame) }}" alt="profile picture">
                                    </div>

                                    <button type="button" onclick="modalProfilePicture()" class="btn btn-secondary btn-pp-style btn-sm position-absolute m-md-2 mb-md-0 mb-lg-3 rounded-circle" data-toggle="modal" data-target="#changePicture" aria-hidden="true"><img src="{{ asset('images/icon_image/camera-solid.png') }}" class="profile_icon_style" alt="profile_picture_icon"></button>
                                @endif
                            </div>

                            <div class="col d-none d-sm-block">
                                <div class="row mb-3">
                                    <div class="col-12">
                                        <h5 class="card-title-profile text-capitalize" style="text-align: left; padding-left: 2px;">
                                            {{ auth()->user()->name }}</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-sm d-none d-sm-block">
                                        <div class="card mb-3 card-level-complete-style">
                                            <div class="card-body p-sm-2 p-md-3 mt-lg-1 p-lg-3">
                                                <h6 class="card-title card-title-level-complete-style"> {{ auth()->user()->level_completed }} Level</h5>
                                                    <p class="card-text card-text-level-complete-style" style="font-size:12px">Level Diselesaikan</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col d-none d-md-block">
                                        <div class="card mb-3 card-level-complete-style" style="width:73%;">
                                            <div class="card-body p-sm-2 p-md-3 mt-lg-1 p-lg-3">
                                                <h6 class="card-title card-title-level-complete-style" style="font-size: 14px" id='login_streak'></h6>
                                                <p class="card-text card-text-level-complete-style" style="font-size:12px">Login Streak</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="mt-4 title-permainan-style">Level Permainan</h2>

            <!-- Game Levels Dasar, Master -->
            @foreach ($game_levels as $level)
                @if ($level->lock_status == 1)
                    <div class="row mt-4 p-2 border level-game-style" style="height:90px; background-color:#{{$level->color_hex}};">
                        <div class="col-3 my-auto">
                            <a href="{{ url('/gameplay/' . $level->id) }}"><i class="fa-solid fa-circle-play ms-2"
                                    style="font-size:35px; color:white !important"></i></a>
                        </div>

                        <div class="col-6 my-auto">
                            <h6 class="mb-0 level-name-style">{{ $level->level_name }} &#40; {{ $level->level_difficulty }} &#41;
                            </h6>
                        </div>

                        <div class="col-3 my-auto">
                            <img class="level-image-style" src="{{ asset($level->image) }}" alt="{{ $level->level_name }}">
                        </div>
                    </div>
                @else
                    <div class="row mt-4 my-auto border text-center level-game-style" style="height:90px; background-color:#AFAFAF !important;">
                        <div class="locked-game-style p-2">
                            <i class="fa-solid fa-lock mt-2" style="font-size:30px;"></i>
                            <p class="mb-0">Terkunci</p>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

    <!-- Popup Profile Picture Modal-->
    <div class="modal fade" id="changePicture" tabindex="-1" role="dialog" aria-labelledby="changePictureModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center mx-auto">Change Profile Picture</h5>
                    <button type="button" class="btn btn-light rounded-circle" onclick="closeModal()" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="/uploadProfilePicture" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <!-- Your form for uploading a new profile picture goes here -->
                        <p>Upload new picture:</p>
                        <input type="file" name="filePP" class="custom-file-input custom-file-input-style" id="fileProfilePicture" aria-describedby="inputFileProfilePicture">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="closeModal()" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Popup Notification Card Modal-->        
    <div class="modal fade" id="popup-modal" tabindex="-1" aria-labelledby="popup-modal">
        <div class="modal-dialog margin-auto">
            <div class="modal-content modal-style-card">

                <div class="card" style="width: 18rem;">
                    <h4 class="card-title pt-4 text-center">Login Streak</h4>
                
                    <div class="card-body modal-style-card-body">
                        <img src="{{ asset('images/streak_login.png') }}" class="card-img-top" alt="streak login image">
                        <h5 id='day_streak_notif'></h5>
                        <p class="card-text" id="login_streak_info"></p>
                        <button id="continue" type="button" class="btn btn-outline-primary" style="width: 100%" data-bs-dismiss="modal">Lanjutkan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="popup-modal-first" tabindex="-1" aria-labelledby="popup-modal-first">
        <div class="modal-dialog margin-auto">
            <div class="modal-content modal-style-card">

                <div class="card" style="width: 25rem;">
                    {{-- <h4 class="card-title pt-4 text-center">Hello!</h4> --}}
                
                    <div class="card-body modal-style-card-body">
                        <img src="{{ asset('images/hello_newuser.jpg') }}" class="card-img-top" alt="streak login image" width="80% !important">
                        <h5 class="text-capitalize">Halo antariksawan, {{auth()->user()->name}}!</h5>
                        <p class="card-text">Ini adalah hari pertamamu pergi ke planet-planet untuk menjelajahi ruang angkasa! Pertama, <b>Aksara</b> di sebelah kanan atas adalah tempat kamu bisa mengetahui aksara Sunda! Test kemampuanmu bisa dilakukan saat kamu menekan <b>Belajar</b> dan menekan tombol <b>Play</b> pada setiap planet!</p>
                        <button id="continue" type="button" class="btn btn-outline-primary" style="width: 100%" data-bs-dismiss="modal">Lanjutkan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="/changepicture" id="changepicture" method="POST">
    @csrf
    <input type="hidden" name="profilePicture" id="profilePicture">
</form>
@endsection

@section('script')
    <script>
        const myModal = new bootstrap.Modal('#popup-modal');
        $( document ).ready(function() {
            axios.get('/attend')
                .then(function(response){
                    // console.log(response.data);
                    document.getElementById('login_streak').innerHTML = response.data.login_streak + ' hari';
                    document.getElementById('day_streak_notif').innerHTML = response.data.login_streak + ' day streak!';
                    document.getElementById('login_streak_info').innerHTML = 'Congrats, ' + response.data.name + '! Kamu berhasil menyelesaikan login streak selama ' + response.data.login_streak + ' hari.';
                });
        });

        function modalProfilePicture() {
            $('#changePicture').modal('show');
        }

        function closeModal() {
            $('#changePicture').modal('hide');
        }
    </script>
    
    @if (auth()->user()->login_streak == 0 && auth()->user()->last_login != $date_now)
        <script>
            $( document ).ready(function() {
                $('#popup-modal-first').modal('show');
            });
        </script>
    @elseif (auth()->user()->last_login != $date_now)
        <script>
            $( document ).ready(function() {
                $('#popup-modal').modal('show');
                console.log(check_absence);
            });
        </script>
    @endif
@endsection