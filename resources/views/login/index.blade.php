@extends('layouts.index')

@section('container')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7 col-sm-8 col-8">
            <main class="form-signin m-auto mt-5 pt-2">
                <h1 class="h3 mb-3 fw-normal text-center">Please sign in</h1>
                
                <form action="/login" method="POST">
                    @csrf
                    <div class="form-floating">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="name@example.com" autofocus required value="{{ old('email') }}">
                        <label for="email">Email address</label>

                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    <div class="pt-3"></div>
                    
                    <div class="form-floating">
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" required>
                        <label for="password">Password</label>

                        @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="pt-3"></div>

                    <!-- Mau di jadiin kaya notification bubble gitu -->
                    @if (session()->has('success'))
                        <p>
                            {{ session('success') }}
                        </p>    
                    @endif

                    @if (session()->has('loginError'))
                        <p>
                            {{ session('loginError') }}
                        </p>    
                    @endif

                    <div class="pt-3"></div>
                    
                    <button class="btn btn-primary w-100 py-2 text-center" type="submit">Login</button>
                </form>
            </main>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        console.log(check_absence);
        check_absence = 0;
    });
</script>
@endsection