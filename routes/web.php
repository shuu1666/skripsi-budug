<?php
// Controllers
use App\Http\Controllers\ExerciseController;
use App\Http\Controllers\FrameController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AksaraController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\StoreController;

//Models
use App\Models\Aksara;
use App\Models\Exercise;
use App\Models\Level;


use Illuminate\Support\Facades\Route;   


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home page
Route::get('/home', [HomeController::class, 'index'])->middleware('auth');

//Profile Picture
Route::post('/uploadProfilePicture', [HomeController::class, 'profilePicture'])->middleware('auth');

Route::get('/', [LoginController::class, 'redirect']);
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);

// Aksara
// mau bikin aksara per halaman
Route::get('/aksara/{aksara:tipe_aksara}', [AksaraController::class, 'index'])->middleware('auth');

//Toko
Route::get('/store', [FrameController::class, 'index'])->middleware('auth');
Route::post('/store', [FrameController::class, 'purchase'])->middleware('auth');
Route::post('/frameuse', [FrameController::class, 'useFrame'])->middleware('auth');


//Game
Route::get('/gameplay/{game:id}', [GameController::class, 'ingameShow'])->middleware('auth');

// Route::get('/testcomplete', [GameController::class, 'testComplete'])->name('data.transfer')->middleware('auth');
Route::post('/levelclear', [GameController::class, 'completeShow'])->name('data.transfer')->middleware('auth');

//attend
Route::get('/attend', [Controller::class, 'attendance'])->middleware('auth');
