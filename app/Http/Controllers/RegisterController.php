<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserGame;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.index', [
            'title' => 'Register',
        ]);
    }

    public function store(Request $request)
    {
        // tangkap semua data yang di post form
        $validatedData = $request->validate([
            'name' => 'required|max:30',
            // 'username' => ['required', 'min:4', 'max:18', 'unique:users'], ------ penulisannya bisa kek gini juga, sama aja
            'username' => 'required|min:4|max:18|unique:users',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:3|max:20',
        ]); 

        // bcrypt buat biar passwordnya ga keliatan di database
        $validatedData['password'] = bcrypt($validatedData['password']);

        $user = User::create($validatedData);

        $unlockGame1 = new UserGame;
        $unlockGame1->user_id = $user->id;
        $unlockGame1->game_id = 1;
        $unlockGame1->lock_status = 1;
        $unlockGame1->save();

        $unlockGame2 = new UserGame;
        $unlockGame2->user_id = $user->id;
        $unlockGame2->game_id = 2;
        $unlockGame2->lock_status = 1;
        $unlockGame2->save();

        // dd($unlockGame1, $unlockGame2);

        return redirect('/login')->with('success', 'Registrasi berhasil! Harap login');
    }
}
