<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Frame;


class HomeController extends Controller
{
    public function levelStatus()
    {
        $user_levels = DB::select('select games.*, users_games.lock_status, users_games.done_status from games left join users_games on users_games.game_id = games.id and users_games.user_id = ' . Auth::user()->id .' order by lock_status DESC, level_difficulty ASC');

        return $user_levels;
    }

    public function index()
    {
        $date_now = Carbon::now()->toDateString();

        // $rank = User::orderBy('total_exp', 'DESC')->get();
        $rank_user_frame = DB::table('users')
        ->select('users.name as name', 'users.image', 'users.total_exp', 'users.used_frame', 'frames.image as frame')
        ->leftJoin('frames', 'users.used_frame', '=', 'frames.id')
        ->orderBy('users.total_exp', 'DESC')
        ->get();
        
        // medal yang dikasih ke user sesuai level complete per user
        if (auth()->user()->level_completed >= 0 && auth()->user()->level_completed < 4) {
            $user_medals = 'images/medals/grey.png';
            $medal_name = 'default';
        } elseif (auth()->user()->level_completed >= 4 && auth()->user()->level_completed < 8) {
            $user_medals = 'images/medals/bronze.png';
            $medal_name = 'bronze';
        } elseif (auth()->user()->level_completed >= 8 && auth()->user()->level_completed < 12) {
            $user_medals = 'images/medals/silver.png';
            $medal_name = 'silver';
        } elseif (auth()->user()->level_completed >= 12) {
            $user_medals = 'images/medals/gold.png';
            $medal_name = 'gold';
        }
        

        $frame_image = null;
        if (Auth::user()->used_frame != null) {
            $frame = Frame::where('id', '=', auth()->user()->used_frame)->first();
            $frame_image = $frame->image;
        }

        $data = [
            'title' => 'Belajar',
            'active' => 'learn',
            'user_profile' => User::all(), //ambil semua user
            'desc_rank' => $rank_user_frame, //ambil rank 3 teratas
            'user_medals' => $user_medals, //medal yang diachive terakhir user
            'date_now' => $date_now,
            'medal_name' => $medal_name,
            'used_frame' => $frame_image,
            'game_levels' => $this->levelStatus(), //level yang udah terbuka apa belom sesuai dimainin user
        ];
        

        return view('index', $data);
    }

    public function profilePicture(Request $request)
    {   
        date_default_timezone_set("Asia/Jakarta");
        $dateImage = date("dmyhis");

        $file = $request->file('filePP');
        $fileName = 'user'.auth()->user()->id.$dateImage.'.jpg';
        // $filePath = public_path('images/user_profile'.$fileName);
        $file->move(public_path("images/user_profile"), $fileName);
        
        $selected_user = User::find(auth()->user()->id);
        $selected_user->image = 'images/user_profile/'.$fileName;
        $selected_user->update();

        return back();
    }
}
