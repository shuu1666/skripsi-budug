<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\UserFrame;

class FrameController extends Controller
{
    public function purchase(Request $request)
    {   
        // dd($request->frame_name, $request->frame_currency);

        // select semua frame di table frames ketika namanya sama dengan request frame
        $selected_frame = DB::table('frames')
                    ->select('id', 'name', 'currency', 'day_streak')
                    ->where('name', '=', $request->frame_name)
                    ->first();

        // select user untuk ngurangin currency
        $user_id = Auth::user()->id;
        $selected_user = User::find($user_id);

        // cari ada purchase story di userframe
        $bought_frame = UserFrame::where('user_id', '=', $user_id)
                            ->where('frame_id', '=', $selected_frame->id)
                            ->first();

        // kalau ga ada history, dia bikin data purchase story dan kurangin currency user
        if ($request->frame_currency && $bought_frame == null) {
            if ($selected_user->currency >= $request->frame_currency) {
                $data_purchased = array('user_id'=>$user_id, 'frame_id'=>$selected_frame->id);
                UserFrame::insert($data_purchased);
    
                $selected_user->currency = $selected_user->currency - $request->frame_currency;
                $selected_user->save();
                
                return redirect('/store');
            }
        }else{
            if ($selected_user->login_streak >= $request->frame_daystreak) {
                $data_purchased = array('user_id'=>$user_id, 'frame_id'=>$selected_frame->id);
                UserFrame::insert($data_purchased);

                return redirect('/store');
            }
        }
        

        return redirect('/store')->withErrors(['message' => 1]);

    }

    public function useFrame(Request $request)
    {
        $user_id = Auth::user()->id;
        $selected_user = User::find($user_id);

        $selected_user->used_frame = $request->frame_id;
        $selected_user->save();

        return redirect('/store');
    }

    public function index() 
    { 
        // select all frames yang dimiliki user sesuai dengan id user dan id frame yang di purchase
        $user_owned_frame = DB::select('select frames.* , users_frames.user_id as purchased from frames left join users_frames on users_frames.frame_id = frames.id AND users_frames.user_id = ' . Auth::user()->id);
  
        return view('mainpage.store', [
            'title' => 'Toko',
            'active' => 'store',
            'list_frame_store' => $user_owned_frame, 
        ]);
    }
}
