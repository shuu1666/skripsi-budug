<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;



class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function attendance()
    {   
        $name_user = Auth::user()->name;
        $last_login = Auth::user()->last_login;
        $date_now = Carbon::now()->toDateString();

        // $message = 'Anda sudah login';

        if ($last_login == null) 
        {
            $user = User::find(Auth::user()->id);
            $user->last_login = $date_now;
            $user->login_streak = 1;
            $user->save();
            // $message = 'Anda berhasil login';
        
        }elseif($last_login != $date_now){
            $user = User::find(Auth::user()->id);
            $user->last_login = $date_now;
            $user->login_streak = $user->login_streak + 1;
            $user->save();
            // $message = 'Anda berhasil login';
        }
    
        // jadi udah update data, pake data terupdatennya
        // jadi kalau udah login if yang di atas ga dipake makanya pake isset buat check usernya ada atau engga
        if(isset($user)) {
            $day_streak = $user->login_streak;
        }else{
            $day_streak = Auth::user()->login_streak;
        }
        
        $data = [
            'login_streak' => $day_streak,
            'name' => $name_user,
            // 'message' => $message,
        ];

        return $data;
    }
}
