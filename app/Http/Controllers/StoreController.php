<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Frame;

class StoreController extends Controller
{
    public function index() 
    {
        // dd(request('frame'));
        return view('mainpage.store', [
            'title' => 'Toko',
            'active' => 'store',
            'list_frame_store' => Frame::all()
        ]);
    }
}
