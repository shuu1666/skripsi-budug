<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Game;
use App\Models\User;
use App\Models\UserGame;

class GameController extends Controller
{
    public function ingameShow(Game $game) 
    {  
        $selected_level = $game->id;
        $selected_level_name = $game->level_name;
        $selected_level_image = $game->image;


        // redirect page
        $check_last_unlock_level = DB::select('select games.id, games.level_name, users_games.lock_status, users_games.done_status from games left join users_games on users_games.game_id = games.id and users_games.user_id = ' . Auth::user()->id .' where games.id = '. $selected_level.' order by lock_status DESC, level_difficulty ASC');

        if($check_last_unlock_level[0]->lock_status == 0) {
            return redirect('/home');
        }
        
        $exercise_level = DB::table('exercises')
                            ->select('exercises.*')
                            ->where('game_id', '=', $selected_level)
                            ->orderBy(DB::raw('RAND()'))
                            ->get();

        // buat iterasi javascript ada berapa drawing canvas
        $exercise_drawing = DB::table('exercises')
                            ->select('exercises.*')
                            ->where('question_type', '=', 'draw')
                            ->get();

        $exercise_anskey = [];
        $counter = 0;
        
        foreach ($exercise_level as $level) {
            // ngasih flag, ans key mana aja yang udah masuk ke array berdasrkan exercise level id
            $check = [$counter]; //buat ngitung jawabannya udah masuk 3 biji atau belom ngikutin for line 47

            // ambil ans key yang asli
            if (isset($level->mean_aksara)) { //kalau ada arti aksara, ambil ke array
                $exercise_anskey[$level->id] = [[$level->answer_key, $level->mean_aksara]];
            } else {
                $exercise_anskey[$level->id] = [[$level->answer_key]];
            }
            
            // ini buat nambahin 2 jawaban selain ans key
            for ($i=0; $i < 2; $i++) { 
                $temp = rand(0,9);

                // while nya ini nanti buat looping sampe beda dari di flag
                // kalau misal yang jawaban ke angka temp ada di angka checkernya, temp nya dirandom lg
                while (in_array($temp, $check)) {
                    $temp = rand(0,9);
                }

                // buat update si check, udah masuk belom jawaban yang salahnya buat pilihan di soalnya
                // kalau diangka check sama temp nya beda baru masuk ke array lagi buat isi jawaban salah
                array_push($check, $temp);

                // ini push isi si jawabannya ke array
                if (isset($level->mean_aksara)) {
                    array_push($exercise_anskey[$level->id], [$exercise_level[$temp]->answer_key, $exercise_level[$temp]->mean_aksara]);
                } else {
                    array_push($exercise_anskey[$level->id], [$exercise_level[$temp]->answer_key]);
                }
                 
            }
            $counter++;
        }

        $data = [
            'title' => 'Game',
            'planet_name' => $selected_level_name,
            'planet_image' => $selected_level_image,
            'exercises' => $exercise_level,
            'exercise_option' => $exercise_anskey,
        ];

        return view('ingame.gameplay', $data);
    }

    public function completeShow(Request $request) 
    {
        $selected_game = UserGame::where('user_id', '=', Auth::user()->id)
                                ->where('game_id', '=', $request->game_id)
                                ->first();

        // --------------------------------------------------------------------------------------
        // deklarasi rewards
        $bonusExp = 0;
        $gainExp = 0;
        $totalStar = $request->gSt;
        $totalExp = $request->gEx;
        $false_answer = $request->fAns;
        $correct_ans = 10-$false_answer;

        // kalau false answernya di bawah 2, dapet bonus exp 2
        if($false_answer <= 2) {
            $bonusExp += 3;
        }

        if ($selected_game->done_status == 0) {
            // kalau game done_statusnya 0, user dapet rewards full, kalau engga, kurang  ------ yang expnya ga full belom ditambahin
            // perhitungan exp
            $gainExp = (int)($request->gEx);

            $totalStar = (int)(($totalStar + 3)/2);
            $totalExp = (int)($totalExp + $bonusExp);

            // update UserGame
            $selected_game->done_status = 1;
            $selected_game->save();
            
        }else{
            $gainExp = (int)(($request->gEx)/2);

            $totalStar = (int)(($totalStar + 3)/3);
            $totalExp = (int)($totalExp/2 + $bonusExp);
        }

        // ---------------------------------------------------------------------------------------
        // perhitungan pendapatan total reward sebelum masuk database
        // data-data yang di update
        // select user sesuai dengan user login
        $selected_user = User::where('id', '=', Auth::user()->id)->first();

        // untuk update banyak level yang sudah diselesaikan oleh user
        $updateCompleted = $selected_user->level_completed + 1;

        // currency
        $updateCurrency = $selected_user->currency + $totalStar;    

        // total exp 
        $updateTotalExp = $selected_user->total_exp + $totalExp; 

        // exp sisa progressbar
        $updateCurrentExp = $updateTotalExp%10;

        // level user
        $updateLevelUser = (int)($updateTotalExp/10);

        $selected_user->currency = $updateCurrency;
        $selected_user->total_exp = $updateTotalExp;
        $selected_user->current_exp = $updateCurrentExp;
        $selected_user->level_user = $updateLevelUser;
        $selected_user->level_completed = $updateCompleted;
        $selected_user->save();

        // ---------------------------------------------------------------------------------------
        // ngambil tulisan level difficultynya buat unlock level berikutnya
        $game_level = Game::find($request->game_id);
        
        // ambil nama level difficulty nya
        $title_game = $game_level->level_difficulty;
        if(substr($title_game, 0, 5) == 'Dasar'){
            $level = (int)substr($title_game, 6);
            $game = substr($title_game, 0, 6);
        }else{
            $level = (int)substr($title_game, 7);
            $game = substr($title_game, 0, 7);
        }

        // nambahin data di UserGame buat unlock
        $next_game_level = $game . ($level+1); // keluarnya tulisan co: "Master 2"
        $next_game = Game::where('level_difficulty', '=', $next_game_level)->first();

        // kalau tidak ada yang sama di table usergame dengan game id dan user id, maka tambah data baru
        // kalau next gamenya tidak null dan gada di database user_game, levelnya berarti nambah data
        if ($next_game != null && $selected_user->level_user%2 == 0) {
            $unlocked_game = DB::select('select user_id, game_id from users_games where user_id = ' . Auth::user()->id .' and game_id = '. $next_game->id);
            
            if ($unlocked_game == null) {
                $user_game = new UserGame();
                $user_game->user_id = auth()->user()->id;
                $user_game->game_id = $next_game->id;
                $user_game->lock_status = 1;
                $user_game->save();
            }
        }

        $data = [
            'title' => 'Game',
            'totalStar' => $totalStar,
            'totalExp' => $totalExp,
            'false_answer'=> $false_answer,
            'correct_ans' => $correct_ans,
        ];

        return view('ingame.complete', $data);
    }

    // public function testComplete()
    // {
    //     $false_answer = 0;
    //     $gainExp = 7;
    //     $bonusExp = 1;
    //     $totalStar = 1;
    //     $totalExp = $gainExp + $bonusExp;

    //     $correct_ans = 10-$false_answer;

    //     $data = [
    //         'title' => 'Game',
    //         'false_answer'=> $false_answer,
    //         'totalStar' => $totalStar,
    //         'totalExp' => $totalExp,
    //         'correct_ans' => $correct_ans,
    //     ];

    //     return view('ingame.complete', $data);
    // }
}
