<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function redirect()
    {
        $url = '';

        if (Auth::user()) {
            $url = '/home';
        } else {
            $url = 'login';
        }
        
        return redirect($url);
    }

    public function index()
    {
        return view('login.index', [
            'title' => 'Login',
        ]);
    }

    public function authenticate(Request $request)
    {   
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        if(Auth::attempt($credentials)) {
            // regenerate pada session merupakan teknik laravel untuk menghindari hacking 'session fixation' dimana hacker akan masuk ke dalam celah keamanan sistem menggunakan sistem, pura pura menggunakan session yang sama 
            $request->session()->regenerate();

            // intended merupakan method yang disediakan laravel yang akan melakukan redirect user ke sebuah tempat URL sebelum melewati autentikasi middleware
            return redirect()->intended('/home');
        }

        return back()->with('loginError', 'Login failed!');
    }

    public function logout()
    {
        Auth::logout();
 
        request()->session()->invalidate();
    
        request()->session()->regenerateToken();
    
        return redirect('/login');
    }
}
