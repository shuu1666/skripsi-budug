<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Aksara;

class AksaraController extends Controller
{
    public function index(Aksara $aksara) 
    {
        return view('mainpage.aksara', [
            'title' => $aksara->tipe_aksara,
            'active' => 'aksara',
            'aksaras' => Aksara::find($aksara->tipe_aksara),
        ]);
    }
}
