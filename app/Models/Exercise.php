<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $fillable = [
        'game_id',
        'question',
        'text_aksara',
        'answer_key',
        'mean_aksara',
    ];

    public function game()
    {
        return $this->belongsTo(Game::class);
    }
}
