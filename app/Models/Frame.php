<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Frame extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function users() 
    {
        return $this->hasMany(UserFrame::class, 'users_frames');
    }
}
