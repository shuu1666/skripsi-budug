<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function games() 
    {
        return $this->hasMany(UserGame::class, 'users_games');
    }
    public function exercises() 
    {
        return $this->hasMany(Exercise::class);
    }
}
