<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aksara extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public static function find($tipe_aksara) {
        $collect_aksara = Aksara::where('tipe_aksara', $tipe_aksara)->get();

        return $collect_aksara;
    }
}
