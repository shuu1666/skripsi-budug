<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFrame extends Model
{
    use HasFactory;

    protected $table = 'users_frames';

    public function frame()
    {
        return $this->belongsTo(Frame::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
